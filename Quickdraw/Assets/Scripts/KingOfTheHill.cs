﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class KingOfTheHill : MonoBehaviour
{

    public ParticleSystem emitter = null;
    public Image progressBar = null;
    public float progress = 0;
    public float progressRate = 0.01f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            progress += progressRate;
            if (emitter != null)
                emitter.Emit(1);
        }
        progressBar.material.SetFloat("_Progress", progress);
        
        if (progress >= 1)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].GetComponent<Enemy>().Death();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            emitter.Stop();
        }
    }
}
