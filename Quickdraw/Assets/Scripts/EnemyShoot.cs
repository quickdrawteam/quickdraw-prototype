﻿using UnityEngine;
using System.Collections;

public class EnemyShoot : MonoBehaviour
{

    [Header("Shooting Options")]
    public float bulletSpread = 1.0f;
    public float bulletRange = 10.0f;
    public int bulletDamage = 45;
    public float reloadSpeed = 4.0f;
    [Tooltip("Time Between Shots")]
    public float fireSpeed = 1.0f;
    [Tooltip("Determines amount of bullets to be fired when the player shoots")]
    public float bulletsPerShot = 10.0f;
    public GameObject raycastOrigin = null;

    public GameObject tracerBullet;
    public GameObject bulletOrigin;
    
    private float shootTimer = 0;
    private float rayCastCounter = 0;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.GetComponent<Enemy>().dead == false)
        {

            if (rayCastCounter <= 0)
            {
                //checks to see if the enemy can see the player
                rayCastCounter = fireSpeed;

                Vector3 direction = GetComponent<Enemy>().player.transform.position - raycastOrigin.transform.position;

                Ray r = new Ray(raycastOrigin.transform.position, direction);
                RaycastHit hit;

                if (Physics.Raycast(r, out hit))
                {
                    //if the player is seen
                    if (hit.collider.gameObject.tag == "Player" || hit.collider)
                    {
                        Debug.DrawLine(raycastOrigin.transform.position, hit.point);
                        ShootBurst();
                        //ShootRay();
                    }
                    else
                    {
                        //Debug.Log(hit.collider.gameObject.name);
                    }
                }
                else
                {
                    //move to new location
                    for (int i = 0; i < 30; ++i)
                        Debug.DrawLine(raycastOrigin.transform.position, hit.point);
                }
            }

            //stops the enemy raycasting every frame to help performance
            if (rayCastCounter > 0)
                rayCastCounter -= Time.deltaTime;
        }
    }

    void ShootRay()
    {

  
        transform.LookAt(GetComponent<Enemy>().player.transform.position);                  //look at the player before shooting forwards

       
        float randomRadius = Random.Range(0, bulletSpread);                                 // ray casts will be in a circle area
        float randomAngle = Random.Range(0, 2 * Mathf.PI);                                  // ray casts will be in a circle area


        Vector3 direction = new Vector3(randomRadius * Mathf.Cos(randomAngle),     
                            randomRadius * Mathf.Sin(randomAngle), bulletRange);            //Calculate raycast direction

        direction = bulletOrigin.transform.TransformDirection(direction.normalized);        //make direction match the transform
                            
                                                                                            // eg. converting the vector3 forward to transform forward

        Ray r = new Ray(raycastOrigin.transform.position, direction);                       //raycast to the player
        RaycastHit hit;

        if ((transform.position - GameObject.FindGameObjectWithTag("Player").gameObject.transform.position).magnitude < 20)
        {
            if (Physics.Raycast(r, out hit))
            {
                if (hit.collider.gameObject.tag == "Player")                                    //if the object hit is the player
                {
                    hit.collider.gameObject.GetComponent<Player>().TakeDamage(bulletDamage);    //tell the player to take damage depending on the enemies damage
                    ShootTracer(direction);                                                     //Shoot visible bullet
                }

            }
        }
    }

    void ShootTracer(Vector3 direction)
    {
        Vector3 origin = bulletOrigin.transform.position;
        GameObject tracer = Instantiate(tracerBullet, origin + direction, transform.rotation) as GameObject;
        tracer.GetComponent<Rigidbody>().AddForce(direction * 100);
        Destroy(tracer, 1);
    }

    void ShootBurst()
    {
        Invoke("ShootRay", 0);
        Invoke("ShootRay", 0.1f);
        Invoke("ShootRay", 0.2f);
    }
}
