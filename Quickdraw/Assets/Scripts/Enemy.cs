﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    [Header("General Options")]
    public float health = 100;                          //Current Health
    public float moveSpeed = 6.0f;                      //The speed at which they move
    public float deathDelay = 0;                        //How long it takes for them to die after health gets below 0
    private bool droppedLoot = false;                   //If they have dropped loot yet
    private bool addedScore = false;                    //If they have given score to the player yet for killing an enemy

    NavMeshAgent navAgent;                              //The navmesh agent
    [HideInInspector]
    public GameObject player;                           //The player GameObject


    public GameObject ammoDrop;                         //What the enemy drops when they die

    private bool inDistance;                            //Whether the player is within distance to start shooting
    public bool dead;                                   //Whether the enemy is dead yet or not

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").gameObject;     //Making the player variable equal the player object
        navAgent = GetComponent<NavMeshAgent>();                            //Making the navagent variable find the nav agent attached to the player
        FindPlayer();                                                       //Tells the enemy to find the player on the navmesh
        dead = false;                                                       //Sets dea to false ( is alive )
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0 && !addedScore)
        {

            //Increases the players score
            GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().score +=
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().killScoreIncrease;

            Death();
            addedScore = true;

        }

        //Check to see if under fire, if so find cover otherwise find and persue player
        FindPlayer();
        CheckDistanceToPlayer();
    }

    void FindPlayer()
    {
        if (!dead)
        {
            if (inDistance)
                navAgent.Stop();
            else
            if (navAgent != null && health > 0)
                navAgent.destination = player.transform.position;
        }
    }

    void FindCover()
    {
        //Find the closest cover if under fire
    }

    void CheckDistanceToPlayer()
    {
        if ((transform.position - player.transform.position).magnitude <= Random.Range(3, 4))
        {
            inDistance = true;
        }
    }

    public void TakeDamage(float Damage)
    {
        health -= Damage;
    }

    public void Death()
    {
        GetComponent<Animator>().SetBool("Alive", false);
        transform.FindChild("Head").GetComponent<Collider>().enabled = false;
        transform.FindChild("Body").GetComponent<Collider>().enabled = false;
        //call this function to drop loot such as weapons and ammo
        int droplootResult = Random.Range(0, 10);
        if (droplootResult > 6 && !droppedLoot)
        {
            Invoke("DropLoot", 1.9f);
            droppedLoot = true;
        }
        navAgent.Stop();

        dead = true;
        //Destroys the object after a set amount of time
        //Gives us time to see the death animation
        Destroy(this.gameObject, 2);
    }

    void DropLoot()
    {
        GameObject newDrop = Instantiate(ammoDrop, transform.position, transform.rotation) as GameObject;
        newDrop.transform.position = transform.position;

    }
}