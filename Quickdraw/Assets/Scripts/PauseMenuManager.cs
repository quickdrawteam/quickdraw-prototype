﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class PauseMenuManager : MonoBehaviour
{
    private bool escapeDown;
    private bool isEscapePressed = false;
    private bool openGUI;
    private float timer;
    public Canvas pauseMenu;
    public Canvas inGameUI;
    
    void Update()
    {
        escapeDown = Input.GetKey(KeyCode.Escape);
        if (escapeDown && !isEscapePressed && !openGUI)
        {
            openGUI = true;
            isEscapePressed = true;
        }
        else if (escapeDown && !isEscapePressed && openGUI)
        {
            openGUI = false;
            isEscapePressed = true;
        }

        if (!escapeDown)
        {
            isEscapePressed = false;
        }

        if (openGUI)
        {
            Time.timeScale = 0;                                                                                         //Sets time scale to 0 ( Nothing gets moved ) 
            pauseMenu.gameObject.SetActive(true);                                                                       //Activates the pause menu
            Camera.main.gameObject.GetComponent<Blur>().enabled = true;                                                 //Activates blur on the main camera
            Camera.main.gameObject.transform.FindChild("Gun Camera").gameObject.SetActive(false);                       //Turns off the Gun Camera
            inGameUI.gameObject.SetActive(false);                                                                       //Disables all game UI
            GameObject.FindGameObjectWithTag("Player").gameObject.GetComponent<RadarSystem>().enabled = false;          //Turns the radar system off   
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Time.timeScale = 1;                                                                                         //Sets time scale to 1 ( Normal time scale )
            pauseMenu.gameObject.SetActive(false);                                                                      //Hides the pause menu
            Camera.main.gameObject.GetComponent<Blur>().enabled = false;                                                //Disables blur on the main camera
            Camera.main.gameObject.transform.FindChild("Gun Camera").gameObject.SetActive(true);                        //Turns the gun camera on
            Camera.main.gameObject.GetComponentInChildren<Camera>().gameObject.GetComponent<Blur>().enabled = false;    //Disables blur on the gun camera
            inGameUI.gameObject.SetActive(true);                                                                        //Activates all game UI
            GameObject.FindGameObjectWithTag("Player").gameObject.GetComponent<RadarSystem>().enabled = true;           //Turns the radar system on
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
