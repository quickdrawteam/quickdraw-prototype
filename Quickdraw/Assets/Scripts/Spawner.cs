﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
	#region Variables
	[Header("Enemy Settings")]
	[Tooltip("The enemy prefab")]
	public GameObject	enemyPrefab;

	[Tooltip ("How often they spawn (In Seconds)")]
	public float		enemySpawnTime;
	[Tooltip("How many enemies in total spawn")]
	public int			enemyCount;

    public GameObject[] spawnNodes;
    
    [HideInInspector]
    public GameObject player;


	//Private Variables
	private GameObject enemyHolder;
	private float	enemySpawnCounter;
	private int enemyTotalCounter;
	#endregion

	void Start()
	{
		enemySpawnCounter = enemySpawnTime;

		//Make sure that 
		enemyHolder = transform.parent.gameObject;

        spawnNodes = GameObject.FindGameObjectsWithTag("SpawnerNode");

        player = GameObject.FindGameObjectWithTag("Player").gameObject;

		//Checks to see if "enemyHolder" or "enemyPrefab" have not been set
		if (enemyHolder == null || enemyPrefab == null)
			Debug.Log("Something Not Set");
	}

	void Update()
	{
		SpawnEnemies(30);
	}

	void SpawnEnemies(int amountOfEnemies)
	{
		enemySpawnCounter -= Time.deltaTime;
		if (enemySpawnCounter <= 0 && enemyTotalCounter != enemyCount)
		{
			GameObject newEnemy = Instantiate(enemyPrefab) as GameObject;

            int randomNodeIndex = GetRandomNode();
            newEnemy.transform.position = spawnNodes[randomNodeIndex].gameObject.transform.position;
			newEnemy.transform.parent = transform.parent;
			enemyTotalCounter += 1;
			enemySpawnCounter = enemySpawnTime;
		}
	}

    int GetRandomNode()
    {
        return Random.Range(0, spawnNodes.Length);
    }
}
