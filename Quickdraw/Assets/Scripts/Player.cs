﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class Player : MonoBehaviour
{
    #region Health Options
    [Header("Health Options")]
    //Can add shield/armour if desired
    public float  health    = 100;        //Current health
    public int    healthRegen = 5;          //How fast your health regens

    private float regenDelay = 3;           //How long it takes from taking damage to regening health
    private float deathDelay = 3.0f;        //How long it takes for the enemies to die
    [Space(5)]
    #endregion

    #region Shooting Options
    [Header("Shooting Options")]
    public float bulletSpread   = 1.0f;     //The angle of spread for bullet firing
    public float aimBulletSpread= 0.001f;   //The angle of spread for bullet firing while aiming
    public float bulletRange    = 10.0f;    //How far the bullets travel
    public float bulletDamage   = 45.0f;    //How much damage bullets do per hit
    public int   magSize        = 30;       //the max amount of bullets in a magazine
    public int   maxAmmo        = 150;      //the max amount of bullets the player can carry at any given time
    public float reloadSpeed    = 4.0f;
    [Tooltip("Time Between Shots")] public float fireSpeed      = 1.0f;
    [Tooltip("Determines amount of bullets to be fired when the player shoots")] public float bulletsPerShot = 10.0f;

    public GameObject bulletOrigin = null;  //Where the rayCast comes from
    public ParticleSystem muzzleFlash;      //Where the muzzle flash comes from

    //private Shooting Variables
    private float initBulletSpread;
    private bool  reloading     = false;    //Check to see if its reloading or not
    private float regenTimer    = 0;
    private float shootTimer    = 0;
    private float reloadTimer   = 0;
    private int   bulletsInMag  = 0;        //tracks the bullets in the current magazine 

    private int ammoCount = 0;              //tracks the total amount of bullets the player is carrying
    
    public GameObject bulletHole;           //The bulletHole that spawns when a wall is shot
    public GameObject tracerBullet;         //The object that comes out of the gun to fake bullets firing

    public Light muzzleLight;               //The light to light up objects surrounding the player when bullets are shot
    public Animator gunAnim;                //Gun Animator
    public List<GameObject> bulletList;     //List of active bullets in the scene
    public AudioSource bulletFireSound;     //The sound the bullets make when they are fired
    [Space(5)]
    #endregion
    
    #region UI Options
    [Header("UI")]
    public Text bulletsText;                //The text on the canvas that represents ammo count
    public Text healthText;                 //The text on the canvas that represents health
    public Text scoreText;                  //The text on the canvas that represents score
    public Image outerReticle;              //The sprite on the canvas that represents the outer reticle ( changes its size )
    public Image healthBar;
    #endregion

    #region Scoring
    public int hitScoreIncrease;            //How many points you get for hitting the enemy
    public int killScoreIncrease;           //How many points you get for killing an enemy
    public int score;                       //Current score
    #endregion
    
    void Start()
    {
        bulletsInMag = magSize;
        ammoCount = maxAmmo;
        reloadTimer = reloadSpeed;
        initBulletSpread = bulletSpread;
        bulletList = new List<GameObject>();
        healthBar.material.SetFloat("_Progress", 1);

    }

    void Update()
    {
        CheckIfAlive();

        RegenHealth();

        AddInaccuracyWithMovement();

        #region Checking Inputs For Shooting
        if (Time.deltaTime > 0)
        {
            //If the player can shoo and the player has pressed the shoot button
            if (shootTimer <= 0 && Input.GetMouseButton(0) && !reloading && bulletsInMag > 0)
            {
                //Shoot and reset the delay
                for (int i = 0; i < bulletsPerShot; ++i)
                    ShootRay();
                shootTimer = fireSpeed;
            }
            else
            {
                gunAnim.SetBool("Shooting", false);
            }
        }
        #endregion

        if (shootTimer > 0)
            shootTimer -= Time.deltaTime;
        if (regenTimer > 0)
            regenTimer -= Time.deltaTime;

        Reload();
        
        UpdateText();


        //Clamping the outer rings of reticle
        outerReticle.transform.localScale = new Vector3(
        Mathf.Clamp(outerReticle.transform.localScale.x, 4, 7),
        Mathf.Clamp(outerReticle.transform.localScale.y, 4, 7),
        Mathf.Clamp(outerReticle.transform.localScale.z, 4, 7));

        if (Time.deltaTime > 0)
        {
            outerReticle.transform.localScale *= 0.99f;
        }

        Camera.main.GetComponent<VignetteAndChromaticAberration>().intensity *= 0.9f;
    }

    void ShootRay()
    {
        gunAnim.SetBool("Shooting", true);
        bulletsInMag -= 1;
        
        outerReticle.transform.localScale *= 2;
        // ray casts will be in a circle area
        float randomRadius = Random.Range(0, bulletSpread);
        float randomAngle = Random.Range(0, 2 * Mathf.PI);

        //Calculate raycast direction
        Vector3 direction = new Vector3(randomRadius * Mathf.Cos(randomAngle), randomRadius * Mathf.Sin(randomAngle), bulletRange);

        //make direction match the transform
        // eg. converting the vector3 forward to transform forward
        direction = Camera.main.transform.TransformDirection(direction.normalized);

        Vector3 origin = bulletOrigin.transform.position;

        //raycast and debug
        Ray r = new Ray(Camera.main.transform.position, direction);

        RaycastHit hit;

        //Main Ray Cast ( Shooting )
        if (Physics.Raycast(r, out hit))
        {
            //for debug only
            Debug.DrawLine(origin, hit.point);
            
            //if the object hit is an enemy
            if (hit.collider.gameObject.tag == "EnemyHead")
            {
                //tell the enemy to take damage depending on the players damage
                hit.collider.gameObject.transform.parent.GetComponent<Enemy>().TakeDamage(bulletDamage * 2);
                score += hitScoreIncrease;
            }

            else if (hit.collider.gameObject.tag == "EnemyBody")
            {
                //tell the enemy to take damage depending on the players damage
                hit.collider.gameObject.transform.parent.GetComponent<Enemy>().TakeDamage(bulletDamage);
                score += hitScoreIncrease;
            }
            else
            {
                //Bullet Hole
                Quaternion hitRotation = Quaternion.LookRotation(-hit.normal);
                GameObject newBulletHole = Instantiate(bulletHole, hit.point, hitRotation) as GameObject;
                newBulletHole.transform.position += newBulletHole.transform.forward * -0.001f;
                bulletList.Add(newBulletHole);


                if (bulletList.Count > 50)
                {
                    Destroy(bulletList[0].gameObject);
                    bulletList.RemoveAt(0);
                }
            }
        }

        //Muzzle Flash
        muzzleFlash.Emit(1);

        //Shoots tracer
        GameObject tracer = Instantiate(tracerBullet, origin + direction, Camera.main.transform.rotation) as GameObject;
        tracer.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 100);
        Destroy(tracer, 1);

    }

    void ReloadWeapon()
    {
        //if you have bullets to reload with
        if (ammoCount > 0)
        {
            //reload
            if (reloadTimer <= 0)
            {
                reloadTimer = reloadSpeed;
                ammoCount -= magSize;
                if (bulletsInMag > 0)
                    ammoCount += bulletsInMag;

                bulletsInMag = magSize;

                if (ammoCount < 0)
                {
                    bulletsInMag += ammoCount;
                    ammoCount -= ammoCount;

                }

                reloading = false;
                gunAnim.SetBool("Reloading", reloading);
            }

            reloadTimer -= Time.deltaTime;
        }
        else
        {
            //display message here telling them to find a new weapon
            reloading = false;
        }

    }

    public void TakeDamage(int damage)
    {
        regenTimer = regenDelay;
        health -= damage;
        Camera.main.GetComponent<VignetteAndChromaticAberration>().intensity = 0.55f;
    }

    void Death()
    {
        Destroy(this.gameObject, 0);
        SceneManager.LoadScene("Main Scene");        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Ammo")
        {
            if (ammoCount < maxAmmo)
            {
                ammoCount += other.gameObject.GetComponent<ItemDrop>().pickUpValue;
                Destroy(other.gameObject);
                if (ammoCount > maxAmmo)
                {
                    ammoCount = maxAmmo;
                }
            }
        }

        if (other.transform.tag == "Health")
        {
            if (health < 100)
            {
                health += other.gameObject.GetComponent<ItemDrop>().pickUpValue;
                Destroy(other.gameObject);

                //If it goes over 100 set it back to 100
                if (health > 100)
                {
                    health = 100;
                }
            }
        }
    }

    void CheckIfAlive()
    {
        //if the player hass no health
        if (health <= 0)
            //kill the player
            Death();
    }

    void RegenHealth()
    {
        if (regenTimer <= 0)
        {
            regenTimer = 1;
            health += healthRegen;
            if (health > 100)
                health = 100;
        }
    }

    void AddInaccuracyWithMovement()
    {
        if (Input.GetMouseButton(1))
            bulletSpread = GetComponent<Rigidbody>().velocity.normalized.magnitude + aimBulletSpread;
        else
            bulletSpread = GetComponent<Rigidbody>().velocity.normalized.magnitude + initBulletSpread;
    }

    void Reload()
    {
        //Reloading
        if (bulletsInMag <= 0)
            reloading = true;
        else if (bulletsInMag < magSize && Input.GetKeyDown(KeyCode.R))
            reloading = true;

        if (reloading)
        {
            ReloadWeapon();
            gunAnim.SetBool("Reloading", reloading);
        }
    }

    void UpdateText()
    {
        //Making the text UI change every frame
        bulletsText.text    = bulletsInMag.ToString() + "/" + ammoCount.ToString();
        healthText.text     = health.ToString() + "/100";
        scoreText.text      = "Score: " + score.ToString();
        healthBar.material.SetFloat("_Progress", health / 100);
    }
}
